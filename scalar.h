

#ifndef _SCALAR_H_
#define _SCALAR_H_
#include <inttypes.h>

int64_t Scalar(int64_t const * const vector1, int64_t const * const vector2, size_t const size) ;



#endif