#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <inttypes.h>
#include "isPrime.h"
bool IsPrime(const int64_t number) 
{
    int64_t divisor;
    for (divisor = 2l; divisor <=sqrt(number); divisor++) 
        if (number % divisor == 0) 		
            return false;
        
    
    return true;
}


