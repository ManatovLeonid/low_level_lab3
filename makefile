compile_flags = gcc -std=c89 -pedantic -Wall -Werror -ansi
	
all: main

main: scalar.o isPrime.o
	$(compile_flags) main.c isPrime.o scalar.o -o main -lm

scalar.o:
	$(compile_flags) scalar.c -o scalar.o -lm

isPrime.o: 
	$(compile_flags) isPrime.c -o isPrime.o -lm
	
