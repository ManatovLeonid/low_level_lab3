

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <inttypes.h>
#include "scalar.h"

int64_t Scalar(int64_t const * const vector1, int64_t const * const vector2, size_t const size) 
{
	size_t i;
	int64_t scalar = 0;

	for (i = 0; i < size; i++) 
		scalar += vector1[i] * vector2[i];
	

	return scalar;
}

