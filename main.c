#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "scalar.h"
#include "isPrime.h"
#include <inttypes.h>



const int64_t g_vector1[] = {1, 2, 3, 4};
const int64_t g_vector2[] = {5, 6, 7, 8};

int main(int argc, char** argv) 
{
	

    int64_t input_number;
    const size_t size_1 = sizeof(g_vector1) / sizeof(g_vector1[0]);
    const size_t size_2 = sizeof(g_vector2) / sizeof(g_vector2[0]);

    if (size_1 != size_2) 
	{
        fputs("error: vectors are different size.\n", stderr);
        return EXIT_FAILURE;
    }

    printf("scalar product %" PRId64 "\n", Scalar(g_vector1, g_vector2, size_1));
    scanf("%" SCNd64 , &input_number);
    puts(IsPrime(input_number) ? "Number is prime" : "Number is not prime");
	

    return 0;
}
